import React from 'react';
import './App.css';
import Input from './AvgWeight/Input';
import DisplayWeight from './AvgWeight/DisplayWeight';
import Summary from './AvgWeight/Summary';

class App extends React.Component {
  constructor() {
    super()
    this.state = {
      persons: [],
      avg: 0
    }
  }

  onAddperson = (people) => {
    console.log(people)
    var persons = this.state.persons
    persons.push(people)
    this.setState({ persons: persons });
    this.getCalculateWeightAvg();
  }

  getCalculateWeightAvg = (persons) => {
    var sum=0 ;
    for (var i = 0; i<this.state.persons.length; i++) {
      console.log(this.state.persons[i])
      sum = sum + parseInt(this.state.persons[i].weight)
    }
    var avg = sum / this.state.persons.length
    this.setState({ avg: avg })
  }


  clearInfo = () => {
    this.setState({ persons: [],avg:"" })
  }

  render() {
    return (
      <div class="container">
        <Input onAddperson={this.onAddperson} />
        <DisplayWeight sendPerson={this.state.persons} />
        <Summary result={this.state.avg} clickClear={this.clearInfo} />
      </div>
    );
  }
}

export default App;
