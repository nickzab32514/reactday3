import React from 'react';

const summary = (props) => {
    return (
        <div>
        <div style={{color:"white", background:"black"}} className="row">
           <h4 >AVG:{props.result}</h4>
        </div>
        <div className="row">
            <button  class="btn btn-outline-secondary" type="button" onClick={props.clickClear}>Clear</button>
        </div>
        </div>
    )
}

export default summary
