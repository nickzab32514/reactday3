import React from 'react';
import './App.css';
import DoInput from './toDoList/DoInput';
import ShowTabletodo from './toDoList/ShowTabletodo';
class App extends React.Component {
  constructor() {
    super()
    this.state = {
      persons: [],
    }
  }

  onAddperson = (ToDo) => {
    console.log(ToDo)
    var persons = this.state.persons
    persons.push(ToDo)
    this.setState({ persons: persons });

  }



  onChargeRemove = (index) => {
    let checkPersons = this.state.persons
    checkPersons[index].check = false
    this.setState(
      { persons: checkPersons });
  }
  removeClear = (index) => {
    let checkPersons = this.state.persons
    checkPersons.splice(index,1)
    this.setState(
      { persons:checkPersons });
  }




  render() {
    return (
      <div class="container">
        <DoInput onAddperson={this.onAddperson} />
        <ShowTabletodo sendPerson={this.state.persons} Remove={this.onChargeRemove} clear={this.removeClear}/>
      </div>
    );
  }
}

export default App;
