import React from 'react';

class ShowTabletodo extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Todo</th>
                        <th scope="col">Todo</th>

                    </tr>
                </thead>
                <tbody>
                    {
                        this.props.sendPerson.length > 0 ?
                            this.props.sendPerson.map((item, index) => {
                                return (
                                    <tr>
                                        <th scope="row">{index + 1}</th>
                                        {
                                            item.check ?
                                                <td >{item.toDo}</td>
                                                :
                                                <td ><s>{item.toDo}</s></td>
                                        }


                                        {
                                            item.check ?
                                                <td><button class="btn btn-outline-secondary" type="button" onClick={() => this.props.Remove(index)}>done</button></td>
                                                :
                                                <td><button class="btn btn-outline-secondary" type="button" onClick={() => this.props.clear(index)}>Remove</button></td>
                                        }
                                    </tr>
                                )
                            })
                            : <h2 style={{ color: "red", textAlign: "End" }}>NO DATA!!!!!!</h2>//ห้ามใช้-ในreactเปลี่ยนเป็นตัวใหญ่
                    }
                </tbody>

            </table>

        );
    }
}

export default ShowTabletodo
