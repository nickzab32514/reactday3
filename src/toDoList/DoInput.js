import React from 'react';

class DoInput extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            toDo: "",
            check: true
        }
    }

    onNameChargde = (event) => {
        this.setState({ toDo: event.target.value }
        )
        // alert(event.target.value)
    }

    onAdd = () => {
        alert("name:" + this.state.toDo)
        const newPerson = {
            toDo: this.state.toDo,check: this.state.check
        };
        console.log(newPerson)
        this.props.onAddperson(newPerson);
    }

    render() {
        return (
            <div>
                
                <h1>To do list</h1>

                <div className="row">
                    <div className="col-5">
                        <input class="form-control form-control-sm" type="text" placeholder="Name" onChange={this.onNameChargde} />
                    </div>
                    <div className="col-2">
                        <button class="btn btn-outline-secondary" type="button" onClick={this.onAdd}>ADD</button>
                    </div>
                </div>

            </div>
        );
    }
}

export default DoInput;
