import React from 'react';

class Clock extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            date: new Date(),
            useUTC: false
        }
        this.chargeFormat=this.chargeFormat.bind(this);
    }

    componentDidMount() {
        this.timer = setInterval(() => this.tick(), 1000);
    }//แสดงผลแล้วทำไรเวลา

    tick() {//update date
        this.setState({
            date: new Date()
        })
    }

    chargeFormat() {//normal function
        this.setState({
            useUTC: !this.state.useUTC
        })
    }

    // chargeFormat=()=> {//arrow function**starndard
    //     this.setState({
    //         useUTC: !this.state.useUTC
    //     })
    // }



    render() {
        return (
            // <h2>{
            //     this.state.useUTC ?//short if เพื่อเลือกว่าจะตรงกับเงื่อนไขไหน
            //         this.state.date.toUTCString()// if this.state.useUTC==true
            //         :
            //         this.state.date.toTimeString()// if this.state.to==false
            // }
            // </h2>
            
            <div>
                {
                this.state.useUTC ?
                <h2>{this.state.date.toUTCString()}</h2>
                    :
                <h2>{this.state.date.toTimeString()}</h2>}
            <button onClick={this.chargeFormat}>ChargeFornmat</button>

            </div>
            
            //แสดงทั้ง2ตัว
            // <div>
            //     <h2>
            //         { this.state.date.toUTCString()}
            //     </h2>
            //     <h2>
            //     {this.state.date.toTimeString()}
            //     </h2>
            // </div>
        )
    }


    componentWillUnmount() {
        clearInterval(this.timer);
    }


}
export default Clock;
