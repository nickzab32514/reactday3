import React from 'react';
// import InputText from './InputText'

class Input extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            name: "",
            weight: 0
        }
    }
    onNameChargde = (event) => {
        this.setState({ name: event.target.value }
        )
        // alert(event.target.value)
    }
    onWeightChargde = (event) => {
        this.setState({ weight: event.target.value }
        )
        // alert(event.target.value)
    }

    onAdd = () => {
        alert("name:" + this.state.name + " weight:" + this.state.weight)
        const newPerson = {
            name: this.state.name,
            weight: this.state.weight
        };
        this.props.onAddperson(newPerson);
    }

    render() {
        return (
            <div>
                <h1>AVG Weight</h1>

                <div className="row">
                    <div className="col-5">
                        <input class="form-control form-control-sm" type="text" placeholder="Name" onChange={this.onNameChargde} />
                    </div>
                    <div className="col-5">
                        <input class="form-control form-control-sm" type="text" placeholder="Weight" onChange={this.onWeightChargde} />
                    </div>
                    <div className="col-2">
                        <button class="btn btn-outline-secondary" type="button" onClick={this.onAdd}>ADD</button>
                    </div>
                </div>

            </div>
        );
    }
}

export default Input;
