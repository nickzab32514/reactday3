import React from 'react';

const DisplayWeight = (props) => {
    console.log(props.sendPerson);
    return (
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Weight</th>
                </tr>
            </thead>
            <tbody>
                {
                    props.sendPerson.length>0?
                    props.sendPerson.map((item, index) => {
                        return (

                            <tr>
                                <th scope="row">{index+1}</th>
                                <td >{item.name}</td>
                                <td>{item.weight}</td>
                            </tr>
                        )
                    })
                    :<h2 style={{color:"red", textAlign:"End"}}>NO DATA!!!!!!</h2>//ห้ามใช้-ในreactเปลี่ยนเป็นตัวใหญ่
                }
            </tbody>

        </table>

    );
}

export default DisplayWeight

